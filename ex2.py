import sys




def isWhiteLine(string):
    return string.isspace()

with open(sys.argv[1],"r") as fd:
    for string in fd:
        if (isWhiteLine(string)==False):
            print(str(string).strip())
fd.close()